﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;

namespace SistemaDeNotas.Formularios.Cambio_Contraseña_Docente
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class FrmCambioContraseñaDocente : UserControl
    {
        public int _idDocente = 0;
        public int username;

        public FrmCambioContraseñaDocente(int _docente, string _username)
        {
            InitializeComponent();
            _idDocente = _docente;
            txtUsuario.Text = _username;
        }

        public void limpiar()
        {
            txtConfirmarContra.Clear();
            txtContraseña.Clear();
           // txtUsuario.Clear();



        }

        public void modificarDocente()
        {
            Model.Docente docente = new Model.Docente();
            
            DaoEncrypt encry = new DaoEncrypt();

            docente.id_docente = _idDocente;
            docente.usuario = txtUsuario.Text;            
            docente.password = encry.Encriptar(txtContraseña.Password.ToString());    

            DAO.DaoDocente daoDocente = new DAO.DaoDocente();
            daoDocente.actualizarContraDocente(docente);
            tbData.Text = "Los datos del docente fueron actalizados";
        }

        #region EVENTOS
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {


            if (txtContraseña.Password.ToString() == txtConfirmarContra.Password.ToString())
            {
                if (!txtContraseña.Password.Equals("") && !txtConfirmarContra.Password.Equals(""))
                {
                    modificarDocente();
                    limpiar();
                }
                else
                {
                    tbData.Text = "La contraseña no puede ir vacia";
                }
            }
            else
            {
                tbData.Text = "Las contraseñas no coinciden";

            }
        }

        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
            limpiar();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        #endregion

    }
}
