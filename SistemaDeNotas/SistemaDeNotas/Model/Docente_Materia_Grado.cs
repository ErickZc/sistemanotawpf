﻿namespace SistemaDeNotas.Model
{
    class Docente_Materia_Grado
    {

        public Docente_Materia_Grado()
        {

        }

        public int idDocente_Materia { get; set; }

        public Docente id_docente { get; set; }

        public Materia id_materia { get; set; }

        public Grado id_grado { get; set; }

        public string estado { get; set; }

        public string fecha { get; set; }


    }
}
