﻿namespace SistemaDeNotas.Model
{
    class Nota
    {
        public Nota()
        {

        }

        public int id_nota { get; set; }

        public Estudiante id_estudiante { get; set; }

        public Materia id_materia { get; set; }

        public decimal actividad1 { get; set; }

        public decimal actividad2 { get; set; }

        public decimal actividad3 { get; set; }

        public decimal examen { get; set; }

        public decimal promedio { get; set; }

        public Periodo id_periodo { get; set; }

    }
}
