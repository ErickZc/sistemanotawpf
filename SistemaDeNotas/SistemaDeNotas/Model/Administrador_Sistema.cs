﻿namespace SistemaDeNotas.Model
{
    class Administrador_Sistema
    {
        public Administrador_Sistema() { }

        public int id_admin { get; set; }

        public string nombre { get; set; }

        public string apellido { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public string telefono { get; set; }

        public string correo { get; set; }

        public string dui { get; set; }

        public string estado { get; set; }

    }
}
